---
marp: true
theme: default
paginate: true
_paginate: false
---

<!--
_class: lead
_footer: 20.06.2023
-->

<style scoped>
footer {
  font-size: 1.6ex;
}
ul {
    font-size: 1.8ex;
}
p {
    font-size: 1.8ex;
    text-align: right;
}
</style>

# Septimus

### Wieloplatformowy organizer osobisty

![bg right:40% 80%](assets/minimalist-septimus-logo.svg)

<br>

- Szymon Wiśniewski
- Jakub Więcek
- Piotr Zajdziński

<br>

Promotor: Dr inż. Anna Grocholewska-Czuryło

<!--
W ramach naszej pracy inżynierskiej wytworzyliśmy relatywnie prostą aplikację do zarządzania drzewem zadań.
-->

---

# Motywacja

* Brak otwartoźródłowego, wieloplatformowego i wystarczająco satysfakcjonującego autorów oprogramowania tego typu.
* Najbliższa ideałowi była aplikacja _Listing It!_ na Androida, autorstwa holenderskiego programisty Ciske Boekelo.

<!--
Teraz chwilę o motywacji.

Osobiście od ponad dekady poszukuję Świętego Graala wśród aplikacji do zarządzania rzeczami do zrobienia i rzeczami, o których nie chciałbym zapomnieć.

Przez lata odkryłem, że w moim przypadku najlepiej nadają się do tego hierarchiczne listy bez limitu głębokości, bo pozwalają zwizualizować w zrozumiały sposób nawet bardzo złożone koncepcje.

Niestety, poszukiwania zakończyły się niepowodzeniem. Jeśli oprogramowanie oferowało taką funkcję, to było albo zbyt proste, albo zbyt skomplikowane. Jeśli zaś było w sam raz, to było płatne. Jeśli było w sam raz i bezpłatne, to zbierało prywatne dane i wyświetlało irytujące reklamy. Jeśli nie miało żadnego z tych problemów, to było np. tylko na Linuxa albo brakowało mu istotnych funkcji i cech. Doświadczenia kolegów są podobne.

Współdzielona frustracja to pierwszy składnik naszej motywacji. Skoro nie mogliśmy znaleźć wystarczająco dobrej aplikacji, to postanowiliśmy stworzyć ją sami.

Oczywiście nie zamierzaliśmy odkrywać koła na nowo, tylko wziąć aplikację najbliższą ideałowi i uzupełnić jej braki.

Tą aplikacją jest Listing It! na Androida, autorstwa holenderskiego programisty Ciske Boekelo. Debiut w 2015, ostatnia aktualizacja w 2020.

Od 8 lat jest ona dla mnie niewysłowioną pomocą w życiu  codziennym i niecodziennym, niemniej ma istotne wady.
-->

---

![bg 95%](assets/listing-it-01.png)
![bg 95%](assets/listing-it-02.png)
![bg 95%](assets/listing-it-03.png)

---

![bg 95%](assets/listing-it-04.png)
![bg 95%](assets/listing-it-05.png)
![bg 95%](assets/listing-it-06.png)

---

# Cel projektu

* Odtworzenie _Listing It!_ w technologii wieloplatformowej _Flutter_, bez funkcji i cech uznanych przez nas za zbędne, za to z dodanymi funkcjami i cechami, których naszym zdaniem brakowało.

* Platformy docelowe to Windows 10, dowolna dystrybucja Linuxa oraz Android w wersji co najmniej 10.

* Dostaliśmy zgodę samego autora.

---

# Zgoda autora _Listing It!_ -- Ciske Boekelo

<br>

![w:1160](assets/zgoda-autora.png)

---

# Podział zadań

### Szymon Wiśniewski -- większość:

* projektu interfejsu,
* wymagań funkcjonalnych i pozafunkcjonalnych,
* architektury,
* instrukcji użytkowania.

---

# Podział zadań

### Jakub Więcek -- większość:

* doboru szczegółowych technologii, narzędzi i koncepcji programistycznych,
* implementacji.

---

# Podział zadań

### Piotr Zajdziński -- większość:

* doboru metod i narzędzi do pracy grupowej,
* organizacji i skomunikowania zespołu,
* scenariuszy testowych oraz ich realizacji.

---

# Interfejs graficzny

---

![bg 64%](assets/ui-root.png)

---

![bg 68%](assets/ui-task.png)

---

![bg 78%](assets/ui-task-details.png)

---

![bg 82%](assets/ui-edit-task.png)

---

![bg 68%](assets/ui-datetime.png)

---

![bg 80%](assets/ui-move-copy.png)

---

![bg 56%](assets/ui-task-actions.jpg)
![bg 56%](assets/ui-sort-modes.jpg)

---

# Technologie

<br>

![w:500](assets/flutter.svg)

<br>

![w:500](assets/dart.svg)

<!--
Flutter to framework języka Dart. Służy do szybkiego wytwarzania wieloplatformowych aplikacji z graficznym interfejsem. Oba są otwartoźródłowymi technologiami Google'a.

Dart umożliwia kompilację z jednego kodu źródłowego do kodu wykonywalnego na Androidzie, iOSie, MacOSie, Windowsie, Linuksie, a także w przeglądarkach, dzięki transpilacji do JavaScripta.

Funkcja deweloperska hot-reload we Flutterze umożliwia błyskawiczne podglądanie efektów zmian w kodzie bez restartu aplikacji. Właśnie po to Dart może być też interpretowany.

Jest to język ogólnego przeznaczenia, zorientowany obiektowo i statycznie typowany, ale oferujący też wiele konstruktów funkcyjnych. Składnią przypomina nowoczesne języki Swift i Kotlin.

Nie jest dedykowany wyłącznie Flutterowi. Można w nim tworzyć też aplikacje konsolowe, serwery i wiele innych, pozbawionych GUI rozwiązań. Można powiedzieć, że jest bardzo uniwersalny.
-->

---

# Dodatkowe paczki

* GetX

* GetStorage

<!--
GetX to najpopularniejsza biblioteka do Fluttera. Oferuje bardzo wydajne zarządzanie stanem, inteligentną iniekcję zależności oraz wiele uproszczeń względem domyślnej składni frameworka. Deweloperzy obiecują utrzymywanie projektu tak długo, jak będzie istniał sam Flutter.

GetStorage to prosta, lekka i synchroniczna baza danych typu klucz-wartość. Dane są zapisywane i odczytywane z pamięci operacyjnej, natomiast w tle są dodatkowo zapisywane do pamięci trwałej. Dzieło twórców GetX.
-->

---

# Koncepcje

- Programowanie asynchroniczne

  ```dart
  Future<void> main() async {
    WidgetsFlutterBinding.ensureInitialized;
    await di.init();
    runApp(const MyApp());
  }
  ```

<!-- 
Funkcja oznaczona jako async (asynchroniczna) czeka na zakończenie instrukcji, przed którą widnieje słowo kluczowe await -- taka instrukcja musi zwracać specjalny obiekt Future.

Oczekiwanie i reakcja na zwrócone dane odbywa się w osobnym izolacie (odpowiedniku wątku w Darcie), dzięki czemu główny izolat może kontynuować pracę. Jest to fundament m.in. interfejsu graficznego, który się nie "blokuje".

Poboczny izolat podejmuje reakcję dopiero wtedy, gdy otrzyma dane lub błąd.

Informuje on wtedy izolat wywołujący (najczęściej -- główny) o swoim zakończeniu lub niepowodzeniu.
-->

---

# Koncepcje

- Iniekcja zależności

  ```dart
  // plik 'global_dependencies.dart':
  Future<void> init() async {
    await Get.putAsync<TaskStorage>(() => TaskStorage.construct(), permanent: true);
    Get.put<TaskExplorerController>(TaskExplorerController(), permanent: true);
  }

  // plik task_explorer.controller.dart
  TaskStorage taskStorage = Get.find();
  ```

<!--
Kiedy jedna klasa polega na drugiej, zazwyczaj wystarczy, że tworzy jej instancję. Czasem jednak jest to zbyt kosztowne albo wręcz niedorzeczne, szczególnie kiedy najlepiej sprawdziłby się jeden obiekt współdzielony przez wiele innych -- tzw. singleton, np. interfejs do bazy danych.

W pliku global_dependencies mamy do czynienia z dwoma takimi singletonami, do których powinien być zapewniony dostęp z dowolnego miejsca w kodzie -- TaskStorage oraz TaskExplorerController. Jest to zagwarantowane przez mechanizm tzw. iniekcji zależności, czyli utworzenia i przechowywania tych singletonów przez globalną, statyczną klasę Get z paczki GetX.

Flaga permanent sprawia, że singleton nie zostanie automatycznie usunięty, kiedy przestaną istnieć jakiekolwiek referencje do niego.

Następnie, np. w pliku task_explorer.controller, możemy w banalny sposób uzyskać dostęp do bazy danych.
-->

---

# Metody i narzędzia pracy zespołowej

* Gitlab

  * Git
  * Kanban
* Facebook Messenger
* Jitsi Meet

<!--
Gitlab to platforma oferująca m.in. hostowanie zdalnego repozytorium Git, bez którego praca zespołowa za pomocą tego narzędzia jest niemożliwa.

Umożliwia też zarządzanie rzeczami do zrobienia w związku z hostowanym projektem, za pomocą tzw. Issues, możliwych do segregowania i kolejkowania na tablicy inspirowanej Kanbanem.

Przydatną funkcją jest też wbudowane Wiki oparte na Markdownie. Pozwala to w łatwy sposób stworzyć i utrzymywać dokumentację projektu.

Git to rozproszony system kontroli wersji, w którym praca odbywa się na tzw. gałęziach commitów, czyli powiązanych łańcuchowo stanach kodu aplikacji dla różnych momentów w czasie.

Najczęściej gałęzie dedykowane są nowym funkcjonalnościom, które po implementacji i przetestowaniu w izolacji są scalane z gałęziami mniej ulotnymi, np. gałęzią do testowania integracyjnego, a następnie z gałęzią z najnowszą stabilną wersją aplikacji.

Dzięki podziałowi na repozytorium lokalne i zdalne, możliwa jest zespołowa praca nad oprogramowaniem.

Kanban jest zwinną metodyką do zarządzania pracą i wizualizacji procesów. Jej centralnym elementem jest tzw. tablica Kanban, której podstawowy wariant składa się z 3 kolumn:
- ”Do zrobienia”,
- ”W trakcie realizacji”,
- ”Zrobione”.

Tablica jest zespołowa i może składać się z więcej niż 3 kolumn. Umieszczane są na niej konkretne zadania, które można przyporządkować określonej osobie. Na początku zadanie znajduje się w kolumnie ”Do zrobienia”, później przechodzi do ”W trakcie realizacji”, a po zakończeniu trafia do ”Zrobione”.

Cechą charakterystyczną Kanbana jest limit zadań dla kolumny "W trakcie realizacji". Zazwyczaj maksymalna liczba to 3. Ma to służyć uniknięciu przytłoczenia i paraliżu oraz sygnalizować potrzebę zwiększenia mocy przerobowych, np. zatrudnienia.

Messengera myślę, że nie trzeba nikomu przedstawiać.

Jitsi Meet to otwartoźródłowa aplikacja internetowa do wideokonferencji. Jej główną zaletą jest prosty interfejs oraz to, że można z niej korzystać za pomocą zarówno smartfona, jak i komputera stacjonarnego. Plusem jest również możliwość tworzenia pokoi i dołączania do nich bez konieczności rejestracji konta. Zestawienie konferencji jest błyskawiczne -- wystarczy stworzyć pokój i wysłać link do osób, które chce się zaprosić do rozmowy.
-->

---

# Przykładowa tablica Kanban

![](assets/kanban.png)

---

# Testy

* manualne,
* akceptacyjne -- oparte na przypadkach użycia,

* osobne dla każdej platformy; środowiska testowe:
  * komputer osobisty z systemem Windows 10 w wersji 21H2, arch. x86_64, 
  * komputer osobisty z systemem Linux Mint 20.2, arch. x86_64,
  * Samsung Galaxy S8 z systemem Android 9, arch. arm64.

<!--
Z uwagi na relatywną prostotę Septimusa, postanowiliśmy ograniczyć się do testów manualnych, w myśl zasady, że nie warto poświęcać 6 godzin na zautomatyzowanie czegoś, co ręcznie zajmie 6 minut.

Każdy test dotyczył jednego przypadku użycia i polegał na realizacji jego głównego scenariusza, a następnie rozszerzeń.

Takie testy akceptacyjne przeprowadzono osobno dla każdej platformy docelowej.

Oto konkretne środowiska testowe.
-->

---

# Wyniki testów

* 29 na 30 scenariuszy zakończyło się sukcesem,

* negatywny wynik jednego był wynikiem braku walidacji pola nazwy przy edycji zadania,
* 100% pokrycia założonych przypadków użycia, spełnienie ok. 97% z nich.

<!--
Spośród 30 testów osobnych przypadków użycia, rezultatem negatywnym skończył się tylko jeden, dla wszystkich platform.

Był on spowodowany brakiem założonej w przypadku użycia walidacji pola nazwy w widoku edycji zadania.

Daje to 100% pokrycia założonych przypadków użycia oraz spełnienie ok. 97% z nich, jeśli uznać negatywny rezultat danego testu za spełnienie zera procent odpowiadającego mu przypadku użycia.
-->

---

# Perspektywy rozwoju

* ciemny motyw,
* responsywność interfejsu względem rozmiaru ekranu,
* adaptacyjność zachowania interfejsu względem platformy docelowej,
* kompilacja do iOSa,
* eksport i import danych do i z formatu `.json`,
* ręczne sortowanie zadań,
* widok wszystkich zbliżających się terminów,
* powiadomienia push o zbliżających się terminach,
* wbudowany kalendarz.